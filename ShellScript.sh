sudo mkdir sample
cd sample
touch sample.txt
echo “Hi! This is just a sample text file created using shell
script.”>>sample.txt
cat sample.txt
grep -o 't' sample.txt | wc -l
chmod o=rwx sample.txt
echo>>”Hi! This is just another sample text added to file.”>>sample.txt
chmod g=r sample.txt
chmod u-rwx sample.txt
touch sample2.txt
(echo "Hi! This is just a sample text file created using shell script." ; echo
"Hi! This is just another sample text added to file.")>sample2.txt
sudo chmod u=rwx sample.txt
seq 1 1000 > sample.txt
head -50 sample.txt
tail -50 sample.txt
touch prog1.txt prog2.txt program.txt code.txt info.txt
find . -maxdepth 1 -name "*prog*" -print
alias listprog='find . -maxdepth 1 -name "*prog*" -print'
