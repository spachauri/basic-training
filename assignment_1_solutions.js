var array=[3,56,7,32,9,14]
var string="u@#d$fkj$#!kds"
var unflatObject={ "flatJSON": false, "i": { "am": { "not": { "so": { "flat": true, "unflat": false } }, "a": "tree" } }, "dates": [ { "day": 1 }, { "day": 8947 } ] }

var obj={ "flatJSON": false, "i.am.not.so.flat": true, "i.am.not.so.unflat": false, "i.am.a": "tree", "dates.0.day": 1, "dates.1.day": 8947 }





// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
  // Write your code here
  let maxval=Number.MIN_VALUE;
  let secmax=Number.MIN_VALUE;
  for(let value of array){
    if(value>maxval){
      secmax=maxval;
      maxval=value;
     
    }
    if(value<maxval&& value>secmax){
      secmax=value;
    }
    
  }
   return secmax;
  
}


secondLargest(array);


// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
  // Write your code here
  const alpha=new Array(26); //creating of array for hashing
  alpha.fill(0);
  answer={};
  
  for(let i=0 ;i<string.length;i++){
    let ascii_value=string.charCodeAt(i);//ascii
    alpha[ascii_value-97]++;//
    
    
  }
  
  for(let i=0 ;i<string.length;i++){
    var char_value=string.charAt(i);
let ascii_value=string.charCodeAt(i);//ascii
    if(ascii_value>=97 && ascii_value<=122)
    {
      
      answer[char_value]=alpha[ascii_value-97];
    
   }
  }
  
 return answer;
}
calculateFrequency(string);

/* another solution
 
 function calculateFrequency(string) {
  const alpha=new Array(26); //creating of array for hashing
  
  answer={};

  for(let i=0 ;i<string.length;i++){
    var charValue=string.charAt(i);//characater in string at index i
    let asciiValue=string.charCodeAt(i);//ascii value of character at index i
    if(asciiValue>=97 && asciiValue<=122)
    {
      alpha[asciiValue-97]=(alpha[asciiValue-97]+1) || 1 ; //updating alpha array
      answer[charValue]=alpha[asciiValue-97] //using alpha array for value
    }
  }
 return answer;
}

*/



// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatObject) {
  // Write your code here
  return flat(unflatObject);
  
  
}
function flat(obj,parent,res={}){
  for(let key in obj){
    let propname=parent?parent+"."+key:key;
    if(typeof obj[key]=='object'){
      flat(obj[key],propname,res);//recursive call with flattenend object
    }
    else{
      res[propname]=obj[key];
    }
  }
  return res;
  
}
flatten(unflatObject);



// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
function unflatten(obj) {
  // Write your code here
  
var result = {}, temp, subStrings, property, i;
    for (property in obj) {
        subStrings = property.split('.');
        temp = result;
        for (i = 0; i < subStrings.length - 1; i++) {
            if (!(subStrings[i] in temp)) {
                if (isFinite(subStrings[i + 1])) { // check if the next key is
                    temp[subStrings[i]] = [];      // an index of an array
                } else {
                    temp[subStrings[i]] = {};      // or a key of an object
                }
            }
            temp = temp[subStrings[i]];
        }
        temp[subStrings[subStrings.length - 1]] = obj[property];
    }
    return result;
}
/* another way

var result = {}
  for (var i in obj) {
    var keys = i.split('.')//array
    keys.reduce(function(r, e, j) {
      return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 == j ? obj[i] : {}) : [])
    }, result)
  }
  return result



*/


unflatten(obj);














