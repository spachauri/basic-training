# Basic Training Solution Submissions
## Contents of the Repo basic_training

The basic_taraining repo consists of 5 trainining modules along with their assignments.
Each day training module consists of a doc and related assignment(if any)

- Docs are in pdf format
- Assignment may be .sh , .js etc files

## Day 0
- Shell commands doc in pdf format (open in google doc ,adobe pdf viewer etc)
- shell script (To run the file use command "./ShellScript.sh)
  >To run the file use command "./ShellScript.sh
- use sudo if facing permission issue 
   >sudo ./ShellScript.sh


## Day1 
- Git basics Doc (pdf) is added
## Day 2
- JS Basics Doc 
- JS Coding Solutions (assignment_1_solutions.js)
  1. To run solutions use online JS compiler paste each solution and give input
  2. Comments are provided for easy understanding and to dry run logics
  3. Expected input value pattern is provided at top of doc for each function parameter
  
## Day 3
  - Assignment Git doc pdf

## Day 4
  - Web Doc 
  - Web Assignment (SWAPI Api Collection)
    [https://api.postman.com/collections/25372731-18604c02-7e9a-4580-a2e9-395321f22647?access_key=PMAT-01GQ1P3SBE8C2N4TFETERYB816]
## Day 5
  - Git advanced Doc
  - Git advanced assignment [https://gitlab.com/spachauri/basic-training]
